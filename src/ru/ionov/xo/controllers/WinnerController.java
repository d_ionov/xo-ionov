package ru.ionov.xo.controllers;

import ru.ionov.xo.model.Field;
import ru.ionov.xo.model.Figure;
import ru.ionov.xo.model.exceptions.InvalidPointException;

import java.awt.*;


public class WinnerController {

    public static Figure checkWinner(final Field field) throws InvalidPointException {

        if( field.getFigure(new Point(0, 0)) != null &&
            field.getFigure(new Point(0, 0)) == field.getFigure(new Point(0, 1)) &&
            field.getFigure(new Point(0, 0)) == field.getFigure(new Point(0, 2))){
            return (Figure) field.getFigure(new Point(0, 0));
        }
        if(field.getFigure(new Point(1, 0)) != null &&
           field.getFigure(new Point(1, 0)) == field.getFigure(new Point(1, 1)) &&
           field.getFigure(new Point(1, 0)) == field.getFigure(new Point(1, 2))){
            return (Figure) field.getFigure(new Point(1, 0));
        }
        if( field.getFigure(new Point(2, 0)) != null &&
            field.getFigure(new Point(2, 0)) == field.getFigure(new Point(2, 1)) &&
            field.getFigure(new Point(2, 0)) == field.getFigure(new Point(2, 2))){
            return (Figure) field.getFigure(new Point(2, 0));
        }
        if( field.getFigure(new Point(0, 0)) != null &&
            field.getFigure(new Point(0, 0)) == field.getFigure(new Point(1, 0)) &&
            field.getFigure(new Point(0, 0)) == field.getFigure(new Point(2, 0))){
            return (Figure) field.getFigure(new Point(0, 0));
        }
        if( field.getFigure(new Point(0, 1)) != null &&
            field.getFigure(new Point(0, 1)) == field.getFigure(new Point(1, 1)) &&
            field.getFigure(new Point(0, 1)) == field.getFigure(new Point(2, 1))){
            return (Figure) field.getFigure(new Point(0, 1));
        }
        if( field.getFigure(new Point(0, 2)) != null &&
            field.getFigure(new Point(0, 2)) == field.getFigure(new Point(1, 2)) &&
            field.getFigure(new Point(0, 2)) == field.getFigure(new Point(2, 2))){
            return (Figure) field.getFigure(new Point(0, 2));
        }
        if( field.getFigure(new Point(0, 0)) != null &&
            field.getFigure(new Point(0, 0)) == field.getFigure(new Point(1, 1)) &&
            field.getFigure(new Point(0, 0)) == field.getFigure(new Point(2, 2))){
            return (Figure) field.getFigure(new Point(0, 0));
        }
        if( field.getFigure(new Point(0, 2)) != null &&
            field.getFigure(new Point(0, 2)) == field.getFigure(new Point(1, 1)) &&
            field.getFigure(new Point(0, 2)) == field.getFigure(new Point(2, 0))){
            return (Figure) field.getFigure(new Point(0, 2));
        }
            return null;
    }

}
