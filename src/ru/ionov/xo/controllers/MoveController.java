package ru.ionov.xo.controllers;

import ru.ionov.xo.model.Field;
import ru.ionov.xo.model.Figure;
import ru.ionov.xo.model.exceptions.AlreadyOccupiedException;
import ru.ionov.xo.model.exceptions.InvalidPointException;

import java.awt.*;

public class MoveController {

    public static void makeMove(final Field<Figure> field, final Figure figure, final Point point) throws AlreadyOccupiedException, InvalidPointException {
        if(field.getFigure(point) != null){
            throw new AlreadyOccupiedException();
        }
        field.setFigure(point, figure);
    }

}
