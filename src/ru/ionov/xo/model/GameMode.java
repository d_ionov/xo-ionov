package ru.ionov.xo.model;

public enum GameMode {

    PlayerVsPlayer, PlayerVsComputer

}
