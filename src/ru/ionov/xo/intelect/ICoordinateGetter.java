package ru.ionov.xo.intelect;

import ru.ionov.xo.model.games.Game;
import ru.ionov.xo.model.exceptions.AlreadyOccupiedException;
import ru.ionov.xo.model.exceptions.InvalidPointException;

import java.awt.*;

public interface ICoordinateGetter {

    Point getMoveCoordinate(final Game game) throws InvalidPointException, AlreadyOccupiedException;

}
