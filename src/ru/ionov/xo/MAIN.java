package ru.ionov.xo;

import ru.ionov.xo.model.games.Game;
import ru.ionov.xo.model.games.XOGame;
import ru.ionov.xo.model.exceptions.InvalidPointException;


public class MAIN {

    public static void main(final String[] args) throws InvalidPointException {
        final Game game = new XOGame();
        game.play();
    }
}
