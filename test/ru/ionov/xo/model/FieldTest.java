package ru.ionov.xo.model;

import org.junit.Test;
import ru.ionov.xo.model.exceptions.AlreadyOccupiedException;
import ru.ionov.xo.model.exceptions.InvalidPointException;

import java.awt.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

public class FieldTest {

    @Test
    public void testGetSize() throws Exception {
        final Field field = new Field(3);
        assertEquals(3, field.getSize());
    }

    @Test
    public void testGetFigure() throws Exception {
        final Field<Figure> field = new Field(3);
        final Point inputPoint = new Point(0, 0);
        final Figure inputFigure = Figure.O;
        field.setFigure(inputPoint, inputFigure);
        final Figure actualFigure = field.getFigure(inputPoint);
        assertEquals(inputFigure, actualFigure);
    }


    @Test
    public void testGetFigureWhenFieldIsNull() throws Exception {
        final Field<Figure> field = new Field(3);
        final Point inputPoint = new Point(0, 0);
        final Figure actualFigure = field.getFigure(inputPoint);
        assertNull(actualFigure);
    }

    @Test
    public void testGetFigureWhenXLessThenZero() throws Exception {
        final Field<Figure> field = new Field(3);
        final Point inputPoint = new Point(-1, 0);
        final Figure inputFigure = Figure.O;
        try{
            field.setFigure(inputPoint, inputFigure);
            fail();
        }catch (final InvalidPointException e){}
    }

    @Test
    public void testGetFigureWhenYLessThenZero() throws Exception {
        final Field<Figure> field = new Field(3);
        final Point inputPoint = new Point(0, -1);
        final Figure inputFigure = Figure.O;
        try{
            field.setFigure(inputPoint, inputFigure);
            fail();
        }catch (final InvalidPointException e){}
    }

    @Test
    public void testGetFigureWhenXMoreThenZero() throws Exception {
        final Field<Figure> field = new Field(3);
        final Point inputPoint = new Point(field.getSize() + 1, 0);
        final Figure inputFigure = Figure.O;
        try{
            field.setFigure(inputPoint, inputFigure);
            fail();
        }catch (final InvalidPointException e){}
    }

    @Test
    public void testGetFigureWhenYMoreThenZero() throws Exception {
        final Field<Figure> field = new Field(3);
        final Point inputPoint = new Point(0, field.getSize() + 1);
        final Figure inputFigure = Figure.O;
        try{
            field.setFigure(inputPoint, inputFigure);
            fail();
        }catch (final InvalidPointException e){}
    }

    @Test
    public void testGetFigureWhenAllCoordinateIncorect() throws Exception {
        final Field<Figure> field = new Field(3);
        final Point inputPoint = new Point(-1, field.getSize() + 1);
        final Figure inputFigure = Figure.O;
        try{
            field.setFigure(inputPoint, inputFigure);
            fail();
        }catch (final InvalidPointException e){}
    }

}
