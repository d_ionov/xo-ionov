package ru.ionov.xo.intelect.xo;

import ru.ionov.xo.intelect.ICoordinateGetter;
import ru.ionov.xo.model.games.Game;
import ru.ionov.xo.model.exceptions.InvalidPointException;

import java.awt.*;
import java.util.Random;

public class RandomCoordinateGetter implements ICoordinateGetter {

    private static final Random RANDOM = new Random();

    @Override
    public Point getMoveCoordinate(final Game game) {
        Random randomX = new Random();
        Random randomY = new Random();

        while(true){
            Point currentPoint = new Point(randomX.nextInt(3), randomY.nextInt(3));
            try {
                if(game.getGameField().getFigure(currentPoint) == null){
                    return currentPoint;
                }
            } catch (InvalidPointException e) {
                e.printStackTrace();
            }
        }
    }

}
