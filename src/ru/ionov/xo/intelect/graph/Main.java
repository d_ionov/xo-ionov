package ru.ionov.xo.intelect.graph;

import ru.ionov.xo.model.Field;
import ru.ionov.xo.model.Figure;
import ru.ionov.xo.model.exceptions.AlreadyOccupiedException;
import ru.ionov.xo.model.exceptions.InvalidPointException;

public class Main {

    public static void main(String[] args) throws AlreadyOccupiedException, InvalidPointException {
        final GraphNode root = new GraphBuilder().build(Figure.X, new Field(3));
        System.out.println(root.getNode().toString());
        GraphHelper.show(root, 0);
    }

}
