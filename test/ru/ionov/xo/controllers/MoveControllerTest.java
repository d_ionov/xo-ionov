package ru.ionov.xo.controllers;

import org.junit.Test;
import ru.ionov.xo.model.Field;
import ru.ionov.xo.model.Figure;
import ru.ionov.xo.model.exceptions.AlreadyOccupiedException;

import java.awt.*;

import static org.junit.Assert.*;

/**
 * Created by dmitriy on 02.07.17.
 */
public class MoveControllerTest {

    @Test
    public void testForOccupiedField() throws Exception {
        Field field = new Field(3);
        Figure firstFigure = Figure.X;
        Point point = new Point(0, 0);
        MoveController.makeMove(field, firstFigure, point);
        try{
            Figure secondFiugre = Figure.X;
            MoveController.makeMove(field, secondFiugre, point);
            fail();
        }catch(AlreadyOccupiedException e){};
    }

}