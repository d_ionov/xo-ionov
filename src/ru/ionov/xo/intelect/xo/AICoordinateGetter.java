package ru.ionov.xo.intelect.xo;

import ru.ionov.xo.controllers.WinnerController;
import ru.ionov.xo.intelect.ICoordinateGetter;
import ru.ionov.xo.model.*;
import ru.ionov.xo.model.exceptions.AlreadyOccupiedException;
import ru.ionov.xo.model.exceptions.InvalidPointException;
import ru.ionov.xo.model.games.Game;

import java.awt.*;
import java.util.*;
import java.util.List;

public class AICoordinateGetter implements ICoordinateGetter {

    private Figure playerFigure;

    @Override
    public Point getMoveCoordinate(final Game game)  throws InvalidPointException, AlreadyOccupiedException{
        playerFigure = game.getFirstPlayer().getFigure();
        Point point;
        point = checkCoordinateForCompWin(game);
        if(point != null) return point;
        point = checkCoordinateForPlayerWin(game);
        if(point != null) return point;
        return getPointForMove(game);
    }

    private Point checkCoordinateForCompWin(final Game game) throws InvalidPointException, AlreadyOccupiedException{
        final Figure computerFigure = game.getFirstPlayer().getFigure() == Figure.X ? Figure.O : Figure.X;
        final Field field = game.getGameField();
        return iterationOnBoard(field, computerFigure);
    }

    private Point checkCoordinateForPlayerWin(final Game game) throws InvalidPointException, AlreadyOccupiedException{
        final Figure playerFigure = game.getFirstPlayer().getFigure();
        final Field field = game.getGameField();
        return iterationOnBoard(field, playerFigure);
    }

    private Point iterationOnBoard(final Field field, final Figure figure) throws InvalidPointException, AlreadyOccupiedException {
        for(int x = 0; x < field.getSize(); x++){
            for(int y = 0; y < field.getSize(); y++){
                final Point testPoint = new Point(x, y);
                if(field.getFigure(testPoint) == null) {
                    field.setFigure(testPoint, figure);
                    if (WinnerController.checkWinner(field) == figure) {
                        field.setFigure(testPoint, null);
                        return new Point(x, y);
                    } else {
                        field.setFigure(testPoint, null);
                    }
                }
            }
        }
        return null;
    }

    private Point getPointForMove(final Game game) throws InvalidPointException, AlreadyOccupiedException {
        final Field field = game.getGameField();
        if(firstMove(field)){
            return getFirstMove(field);
        }else{
            return getFreePoint(game);
        }
    }

    private Figure checkCenter(final Field field) throws InvalidPointException {
        return (Figure) field.getFigure(new Point(1,1));
    }

    private boolean firstMove(final Field field) throws InvalidPointException, AlreadyOccupiedException {
        return field.getCountFiguresOnBoard() == 0 || field.getCountFiguresOnBoard() == 1;
    }

    private Point getFirstMove(final Field field) throws InvalidPointException {
        final Point[] degreePosiotion = {new Point(), //заглушка
                                         new Point(0,2),
                                         new Point(0,0),
                                         new Point(2,0),
                                         new Point(2,2)};
        if(checkCenter(field) == null) return new Point(1,1);
        else return degreePosiotion[new Random().nextInt(4)];
    }

    private boolean firstRowIsClean(final Field field) throws InvalidPointException {
        return field.getFigure(new Point(0, 0)) != playerFigure &&
               field.getFigure(new Point(1, 0)) != playerFigure &&
               field.getFigure(new Point(2, 0)) != playerFigure;
    }

    private boolean secondRowIsClean(final Field field) throws InvalidPointException {
        return field.getFigure(new Point(0, 1)) != playerFigure &&
               field.getFigure(new Point(1, 1)) != playerFigure &&
               field.getFigure(new Point(2, 1)) != playerFigure;
    }

    private boolean thirdRowIsClean(final Field field) throws InvalidPointException {
        return field.getFigure(new Point(0, 2)) != playerFigure &&
               field.getFigure(new Point(1, 2)) != playerFigure &&
               field.getFigure(new Point(2, 2)) != playerFigure;
    }

    private boolean firstColumnIsClean(final Field field) throws InvalidPointException {
        return field.getFigure(new Point(0, 0)) != playerFigure &&
               field.getFigure(new Point(0, 1)) != playerFigure &&
               field.getFigure(new Point(0, 2)) != playerFigure;
    }

    private boolean secondColumnIsClean(final Field field) throws InvalidPointException {
        return field.getFigure(new Point(1, 0)) != playerFigure &&
               field.getFigure(new Point(1, 1)) != playerFigure &&
               field.getFigure(new Point(1, 2)) != playerFigure;
    }

    private boolean thirdColumnIsClean(final Field field) throws InvalidPointException {
        return field.getFigure(new Point(2, 0)) != playerFigure &&
               field.getFigure(new Point(2, 1)) != playerFigure &&
               field.getFigure(new Point(2, 2)) != playerFigure;
    }

    private HashSet getFreeVectors(final Field field) throws InvalidPointException {
        final HashSet<String> vektors = new HashSet<>();

        if(firstRowIsClean(field)){
            vektors.add("firstRow");
        }

        if(secondRowIsClean(field)){
            vektors.add("secondRow");
        }

        if(thirdRowIsClean(field)){
            vektors.add("thirdRow");
        }

        if(firstColumnIsClean(field)){
            vektors.add("firstColumn");
        }

        if(secondColumnIsClean(field)){
            vektors.add("secondColumn");
        }

        if(thirdColumnIsClean(field)){
            vektors.add("thirdColumn");
        }

        return vektors;
    }

    private Point getFreePoint(final Game game) throws InvalidPointException {
        final Field field = game.getGameField();
        final HashSet<String> vektors = getFreeVectors(field);
        if(!vektors.isEmpty()) {
            List<Point> points = new ArrayList<>();
            if (vektors.contains("firstRow")) {
                points.add(new Point(new Random().nextInt(2), 0));
            }
            if (vektors.contains("secondRow")) {
                points.add(new Point(new Random().nextInt(2), 1));
            }
            if (vektors.contains("thirdRow")) {
                points.add(new Point(new Random().nextInt(2), 2));
            }
            if (vektors.contains("firstColumn")) {
                points.add(new Point(0, new Random().nextInt(2)));
            }
            if (vektors.contains("secondColumn")) {
                points.add(new Point(1, new Random().nextInt(2)));
            }
            if (vektors.contains("thirdColumn")) {
                points.add(new Point(2, new Random().nextInt(2)));
            }
            return points.get(new Random().nextInt(points.size()));
        }else{
            return new RandomCoordinateGetter().getMoveCoordinate(game);
        }
    }

}
