package ru.ionov.xo.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class FigureTest {

    @Test
    public void getFigure() throws Exception {
        final Figure inputFigure = Figure.O;
        assertEquals("O", inputFigure.getFigure());
    }

}