package ru.ionov.xo.intelect.graph;

import ru.ionov.xo.model.Field;

import java.util.Set;

public class GraphNode {

    private final Field node;

    private final Set<GraphNode> children;

    public GraphNode(final Field field, Set<GraphNode> children) {
        this.node = field;
        this.children = children;
    }

    public Field getNode() {
        return node;
    }

    public Set<GraphNode> getChildren() {
        return children;
    }
}
