package ru.ionov.xo.controllers;

import ru.ionov.xo.model.Field;
import ru.ionov.xo.model.Figure;
import ru.ionov.xo.model.exceptions.InvalidPointException;

import java.awt.*;

public class CurrentMoveController {

    public static Figure checkCurrentMove(final Field<Figure> field) throws InvalidPointException {
        int countFigure = 0;
        for(int x = 0; x < field.getSize(); x++){
            for(int y = 0; y < field.getSize(); y++){
                if(field.getFigure(new Point(x, y))  != null){
                    countFigure++;
                }
            }
        }
        if(countFigure % 2 == 0) return Figure.X;
        else return Figure.O;
    }

}
