package ru.ionov.xo.controllers;

import org.junit.Test;
import ru.ionov.xo.model.Field;
import ru.ionov.xo.model.Figure;

import java.awt.*;

import static org.junit.Assert.*;

public class WinnerControllerTest {

    @Test
    public void checkWinnerWhenFirstColumn() throws Exception {
        final Field field = new Field(3);
        field.setFigure(new Point(0, 0), Figure.O);
        field.setFigure(new Point(0, 1), Figure.O);
        field.setFigure(new Point(0, 2), Figure.O);
        assertEquals(Figure.O, WinnerController.checkWinner(field));
    }

    @Test
    public void checkWinnerWhenSecondColumn() throws Exception {
        final Field field = new Field(3);
        field.setFigure(new Point(1, 0), Figure.O);
        field.setFigure(new Point(1, 1), Figure.O);
        field.setFigure(new Point(1, 2), Figure.O);
        assertEquals(Figure.O, WinnerController.checkWinner(field));
    }

    @Test
    public void checkWinnerWhenThirdColumn() throws Exception {
        final Field field = new Field(3);
        field.setFigure(new Point(2, 0), Figure.O);
        field.setFigure(new Point(2, 1), Figure.O);
        field.setFigure(new Point(2, 2), Figure.O);
        assertEquals(Figure.O, WinnerController.checkWinner(field));
    }

    @Test
    public void checkWinnerWhenFirstRow() throws Exception {
        final Field field = new Field(3);
        field.setFigure(new Point(0, 0), Figure.O);
        field.setFigure(new Point(1, 0), Figure.O);
        field.setFigure(new Point(2, 0), Figure.O);
        assertEquals(Figure.O, WinnerController.checkWinner(field));
    }

    @Test
    public void checkWinnerWhenSecondRow() throws Exception {
        final Field field = new Field(3);
        field.setFigure(new Point(0, 1), Figure.O);
        field.setFigure(new Point(1, 1), Figure.O);
        field.setFigure(new Point(2, 1), Figure.O);
        assertEquals(Figure.O, WinnerController.checkWinner(field));
    }

    @Test
    public void checkWinnerWhenThirdRow() throws Exception {
        final Field field = new Field(3);
        field.setFigure(new Point(0, 2), Figure.O);
        field.setFigure(new Point(1, 2), Figure.O);
        field.setFigure(new Point(2, 2), Figure.O);
        assertEquals(Figure.O, WinnerController.checkWinner(field));
    }

    @Test
    public void checkWinnerWhenFirstDiagonal() throws Exception {
        final Field field = new Field(3);
        field.setFigure(new Point(0, 0), Figure.O);
        field.setFigure(new Point(1, 1), Figure.O);
        field.setFigure(new Point(2, 2), Figure.O);
        assertEquals(Figure.O, WinnerController.checkWinner(field));
    }

    @Test
    public void checkWinnerWhenSecondDiagonal() throws Exception {
        final Field field = new Field(3);
        field.setFigure(new Point(2, 0), Figure.O);
        field.setFigure(new Point(1, 1), Figure.O);
        field.setFigure(new Point(0, 2), Figure.O);
        assertEquals(Figure.O, WinnerController.checkWinner(field));
    }

    @Test
    public void checkWinnerWhenNotWinner() throws Exception {
        final Field field = new Field(3);
        field.setFigure(new Point(1, 1), Figure.O);
        field.setFigure(new Point(0, 2), Figure.O);
        assertNull(WinnerController.checkWinner(field));
    }

}