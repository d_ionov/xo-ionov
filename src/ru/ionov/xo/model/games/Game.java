package ru.ionov.xo.model.games;

import ru.ionov.xo.model.Field;
import ru.ionov.xo.model.Figure;
import ru.ionov.xo.model.GameMode;
import ru.ionov.xo.model.Player;
import ru.ionov.xo.model.exceptions.InvalidPointException;
import ru.ionov.xo.view.ConsoleView;

import java.lang.reflect.Array;
import java.util.*;

public abstract class Game implements Iterable<Player> {

    private final Player[] players = new Player[2];

    private final GameMode mode = chooseGameMode();

    private Field<Figure> field;

    public void play(){
        try {
            while (continueGame(field)){
                move(field);
                ConsoleView.show(this);
            }
        } catch (InvalidPointException e) {
            e.printStackTrace();
        }
    }

    public Field getGameField() {
        return field;
    }

    public GameMode getMode(){
        return mode;
    }

    public Player getFirstPlayer(){
        return players[0];
    }

    public Player getSecondPlayer(){
        return players[1];
    }

    void setGameField(final Field<Figure> field) {
        this.field = field;
    }

    void setFirstPlayer(final Player firstPlayer){
        players[0] = firstPlayer;
    }

    void setSecondPlayer(final Player secondPlayer){
        players[1] = secondPlayer;
    }

    protected abstract boolean continueGame(final Field<Figure> field) throws InvalidPointException;

    protected abstract void move(final Field<Figure> field);

    protected abstract void setPlayers();

    private GameMode chooseGameMode(){
        final Scanner scanner = new Scanner(System.in);
        System.out.println("Choose game mode:\n1.Player vs Player\n2.Player vs Computer");
        final String mode = scanner.nextLine();
        if(!mode.equals("1") &&  !mode.equals("2")){
            System.out.println("You type illegal number");
            chooseGameMode();
        }if (Integer.valueOf(mode) == 1){
            return GameMode.PlayerVsPlayer;
        }else{
            return GameMode.PlayerVsComputer;
        }
    }

    @Override
    public Iterator<Player> iterator() {
        return new PlayerIterator(players);
    }

    private static class PlayerIterator implements Iterator<Player>{

        private int index = 0;

        private final Player[] players;

        PlayerIterator(final Player[] players){
            this.players = players;
        }

        @Override
        public boolean hasNext() {
            return index < players.length;
        }

        @Override
        public Player next() {
            int currentIndex = index;
            if(currentIndex == players.length) throw new NoSuchElementException();
            index++;
            return players[index++];
        }
    }
}
