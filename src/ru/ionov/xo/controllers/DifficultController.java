package ru.ionov.xo.controllers;

import ru.ionov.xo.intelect.xo.AICoordinateGetter;
import ru.ionov.xo.intelect.ICoordinateGetter;
import ru.ionov.xo.intelect.xo.RandomCoordinateGetter;
import ru.ionov.xo.model.Difficult;
import ru.ionov.xo.model.GameMode;
import ru.ionov.xo.model.games.Game;

import java.util.Scanner;

public class DifficultController {

    private ICoordinateGetter coordinateGetter;

    public void createICoordinateGetter(Game game){
        if(game.getMode() == GameMode.PlayerVsComputer){
            coordinateGetter = setCoordinateGetter();
        }
    }

    public ICoordinateGetter getCoordinateGetter() {
        return coordinateGetter;
    }

    private ICoordinateGetter setCoordinateGetter(){
        if(new DifficultController().checkDifficult() == Difficult.Easy) return new RandomCoordinateGetter();
        else return new AICoordinateGetter();
    }

    private Difficult checkDifficult(){
        final Difficult difficult;
        System.out.println("Choose difficult: Easy, Medium");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        try{
            difficult = Difficult.valueOf(input);
            return difficult;
        }catch (IllegalArgumentException e){
            System.out.println("Illegal argument. Please repeat input");
            checkDifficult();
        }
        return null;
    }

}
