package ru.ionov.xo.model.games;

import ru.ionov.xo.controllers.CurrentMoveController;
import ru.ionov.xo.controllers.DifficultController;
import ru.ionov.xo.controllers.MoveController;
import ru.ionov.xo.controllers.WinnerController;
import ru.ionov.xo.intelect.ICoordinateGetter;
import ru.ionov.xo.model.Field;
import ru.ionov.xo.model.Figure;
import ru.ionov.xo.model.GameMode;
import ru.ionov.xo.model.Player;
import ru.ionov.xo.model.exceptions.AlreadyOccupiedException;
import ru.ionov.xo.model.exceptions.InvalidPointException;
import ru.ionov.xo.view.ConsoleView;

import java.awt.*;
import java.util.Scanner;

public class XOGame extends Game {

    private final DifficultController difficultController = new DifficultController();

    public XOGame(){
        difficultController.createICoordinateGetter(this);
        setPlayers();
        setGameField(new Field<>(3));
        ConsoleView.show(this);
    }

    @Override
    public boolean continueGame(final Field field) throws InvalidPointException {
        if(WinnerController.checkWinner(field) != null) {
            System.out.format("Winner is %s", WinnerController.checkWinner(field));
            return false;
        }
        if (field.isFull()) System.out.println("No winners!");
        return !field.isFull();
    }

    @Override
    public void move(final Field<Figure> field) {
        if(getMode() == GameMode.PlayerVsPlayer){
            movePvP(field);
        }else {
            movePvC(field);
        }
    }

    @Override
    protected void setPlayers() {
        GameMode mode = getMode();
        if(mode == GameMode.PlayerVsPlayer) {
            String firstPlayerName = inputName();
            Figure firstPlayerFigure = inputFigure();
            setFirstPlayer(new Player(firstPlayerName, firstPlayerFigure));
            String secondPlayerName = inputName();
            Figure secondPlayerFigure = firstPlayerFigure == Figure.X ? Figure.O : Figure.X;
            setSecondPlayer(new Player(secondPlayerName, secondPlayerFigure));
        }
        else{
            String firstPlayerName = inputName();
            Figure inputFigure = inputFigure();
            setFirstPlayer(new Player(firstPlayerName, inputFigure));
        }
    }

    private void movePvP(final Field<Figure> field){
        final Figure currentFigure;
        try {
            currentFigure = CurrentMoveController.checkCurrentMove(field);
            System.out.format("Move player with figure %s", currentFigure);
            System.out.println("");
            System.out.print("Type coordinate X: ");
            int x = applyCoordinate();
            System.out.print("Type coordinate Y: ");
            int y = applyCoordinate();
            final Point point = new Point(x, y);
            MoveController.makeMove(field, currentFigure, point);
        } catch (InvalidPointException e) {
            System.out.println("You typed illegal coordinated. Please type correct X and Y.");
        } catch (AlreadyOccupiedException e) {
            System.out.println("This point already occupied. Please choose another point.");
        }
    }

    private void movePvC(final Field<Figure> field){
        final Figure currentFigure;
        try{
            currentFigure = CurrentMoveController.checkCurrentMove(field);
            if(currentFigure == getFirstPlayer().getFigure()) {
                System.out.format("Move player with figure %s", currentFigure);
                System.out.println("");
                System.out.print("Type coordinate X: ");
                final int x = applyCoordinate();
                System.out.print("Type coordinate Y: ");
                final int y = applyCoordinate();
                final Point point = new Point(x, y);
                MoveController.makeMove(field, currentFigure, point);
            }else{
                ICoordinateGetter coordinateGetter = difficultController.getCoordinateGetter();
                Point point = coordinateGetter.getMoveCoordinate(this);
                System.out.println("Computer make move..");
                MoveController.makeMove(field, currentFigure, point);
            }
        }catch (InvalidPointException e) {
            System.out.println("You typed illegal coordinated. Please type correct X and Y.");
        } catch (AlreadyOccupiedException e) {
            System.out.println("This point already occupied. Please choose another point.");
        }
    }

    private int applyCoordinate(){
        final Scanner scanner = new Scanner(System.in);
        return scanner.nextInt()-1;
    }

    private String inputName(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Type player name: ");
        String playerName = scanner.nextLine();
        return playerName;
    }

    private Figure inputFigure(){
        final Scanner scanner = new Scanner(System.in);
        System.out.println("Choose figure: X or O");
        String inputFigure = scanner.nextLine();
        while(!inputFigure.equals("O") && !inputFigure.equals("X")){
            System.out.println("You type incorrect figure. Please repeat your choose");
            inputFigure = scanner.nextLine();
        }
        return Figure.valueOf(inputFigure);
    }

}
