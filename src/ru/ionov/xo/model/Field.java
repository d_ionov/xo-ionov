package ru.ionov.xo.model;

import ru.ionov.xo.model.exceptions.AlreadyOccupiedException;
import ru.ionov.xo.model.exceptions.InvalidPointException;

import java.awt.*;

public class Field<T> {

    private final int FIELD_SIZE;

    private final int MIN_COORDINATE = 0;

    private final T[][] field;

    public Field(int FIELD_SIZE){
        this.FIELD_SIZE = FIELD_SIZE;
        field = (T[][])new Object[FIELD_SIZE][FIELD_SIZE];
    }

    public Field(final Field inputField){

        /**
         * FIELD_SIZE = inputField;
         * field = inputField.getField();
         */


        FIELD_SIZE = inputField.getSize();
        field = (T[][])new Object[FIELD_SIZE][FIELD_SIZE];
        for(int i = 0; i < 3; i++){
            System.arraycopy(inputField.field[i], 0, field[i], 0, FIELD_SIZE);
        }
    }

    public int getSize(){
        return FIELD_SIZE;
    }

    public boolean isFull(){
        return getCountFiguresOnBoard() == FIELD_SIZE * FIELD_SIZE;
    }

    public int getCountFiguresOnBoard(){
        int count = 0;
        for(int x = 0; x < FIELD_SIZE; x++){
            for(int y = 0; y < FIELD_SIZE; y++){
                try {
                    if(getFigure(new Point(x, y)) != null){
                        count++;
                    }
                } catch (InvalidPointException e) {
                    e.printStackTrace();
                }
            }
        }
        return count;
    }

    public T getFigure(final Point point) throws InvalidPointException {
        if(!checkPoint(point)){
            throw new InvalidPointException();
        }
        return field[point.x][point.y];
    }

    public void setFigure(final Point point, final T t) throws InvalidPointException, AlreadyOccupiedException {
        if(!checkPoint(point)){
            throw new InvalidPointException();
        }
        field[point.x][point.y] = t;
    }

    private boolean checkPoint(Point point){
        return checkCoordinate(point.x) && checkCoordinate(point.y);
    }

    private boolean checkCoordinate(final int coordinate){
        return coordinate >= MIN_COORDINATE && coordinate < FIELD_SIZE;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (int y = 0; y < FIELD_SIZE; y++){
            for(int x = 0; x < FIELD_SIZE; x++){
                sb.append(field[x][y] != null ? field[x][y] : " ");
                if(x != 2){
                    sb.append("|");
                }else{
                    sb.append("\n");
                }
            }
            if(y != 2){
                sb.append("-----\n");
            }

        }
        return sb.toString();
    }
}
