package ru.ionov.xo.model;

import static org.junit.Assert.assertEquals;

public class PlayerTest {
    @org.junit.Test
    public void getName() throws Exception {

        final String inputValue = "Dima";
        final String expectedValue = inputValue;

        final Player player = new Player(inputValue, null);

        final String actualValue = player.getName();

        assertEquals(expectedValue, actualValue);

    }

    @org.junit.Test
    public void getFigure() throws Exception {

        final Figure inputFigure = Figure.X;
        final Figure expectedFigure = inputFigure;

        final Player player = new Player(null, inputFigure);

        final Figure actualFigure = player.getFigure();

        assertEquals(actualFigure, inputFigure);


    }

}
