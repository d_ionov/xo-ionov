package ru.ionov.xo.intelect.graph;

import ru.ionov.xo.model.Field;
import ru.ionov.xo.view.ConsoleView;

public class GraphHelper {

    public static void show(final GraphNode node, final int level){
        final StringBuilder sb = new StringBuilder();
        for(int i = 0; i < level * 7; i++){
            sb.append(".");
        }
        sb.append("  ");
        final String space = sb.toString();
        final String nodeStr = node.getNode().toString();
        final String updateNodeStr = nodeStr.replace("\n", "\n" + space);
        System.out.println("\n" + space + updateNodeStr);
        for(GraphNode child : node.getChildren()){
            show(child, level + 1);
        }
    }

}
