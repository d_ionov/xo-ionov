package ru.ionov.xo.controllers;

import org.junit.Test;
import ru.ionov.xo.model.Field;
import ru.ionov.xo.model.Figure;

import java.awt.*;

import static org.junit.Assert.*;

public class CurrentMoveControllerTest {

    @Test
    public void checkCurrentMove() throws Exception {
        final Field field = new Field(3);
        field.setFigure(new Point(0, 0), Figure.X);
        final Figure actualFigure = CurrentMoveController.checkCurrentMove(field);
        assertEquals(Figure.O, actualFigure);
    }

    @Test
    public void checkCurrentMoveWhenFirstMove() throws Exception {
        final Field field = new Field(3);
        final Figure actualFigure = CurrentMoveController.checkCurrentMove(field);
        assertEquals(Figure.X, actualFigure);
    }

}