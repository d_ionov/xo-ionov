package ru.ionov.xo.view;

import ru.ionov.xo.model.Field;
import ru.ionov.xo.model.Figure;
import ru.ionov.xo.model.Player;
import ru.ionov.xo.model.exceptions.InvalidPointException;
import ru.ionov.xo.model.games.Game;

import java.awt.*;


public class ConsoleView {

    public static void show(final Game game){
        Field<Figure> field = game.getGameField();
        print(field);
    }

    private static void print(final Field<Figure> field){
        System.out.println(field.toString());
    }

}
