package ru.ionov.xo.intelect.graph;

import ru.ionov.xo.model.Field;
import ru.ionov.xo.model.Figure;
import ru.ionov.xo.model.exceptions.AlreadyOccupiedException;
import ru.ionov.xo.model.exceptions.InvalidPointException;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

public class GraphBuilder {

    public GraphNode build(final Figure currentFigure, final Field currentField) throws InvalidPointException,
                                                                                        AlreadyOccupiedException {
        final Figure nextFigure = currentFigure == Figure.O ? Figure.X : Figure.O;
        final Set<GraphNode> children = new HashSet<>();
        for(int y = 0; y < currentField.getSize(); y++){
            for(int x = 0; x < currentField.getSize(); x++){
                if(currentField.getFigure(new Point(x, y)) != null){
                    continue;
                }
                final Field newField = new Field(currentField);
                newField.setFigure(new Point(x, y), nextFigure);
                children.add(build(nextFigure, newField));
            }
        }
        return new GraphNode(currentField, children);
    }

}
