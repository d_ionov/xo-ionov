package ru.ionov.xo.model;

public enum Difficult {

    Easy("Easy"), Medium("Medium");

    private final String difficult;

    Difficult(String difficult){
        this.difficult = difficult;
    }

}
